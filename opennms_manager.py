#!/usr/bin/env python
"""
Provide some helper functionality for bulk dump/loading/transform of nodes/requisitions in OpenNMS
"""

import json, re, subprocess, sys, time, urllib, urllib2

class OpenNMS:
    "Provides an interface to an OpenNMS host's ReST API"

    REALM = "OpenNMS Realm"
    DEFAULT_HEADERS = {"Accept": "application/json"}

    def __init__(self, scheme="http", host="localhost", port=8980, user=None, password=None, api_prefix="opennms/rest/"):
        self.scheme = scheme
        self.host = host
        self.port = port
        self.api_prefix = api_prefix
        self.url = "{scheme}://{host}:{port}".format(scheme=self.scheme, host=self.host, port=self.port)

        if not user:
            self.opener = urllib2.build_opener()
        else:
            # Setup credentials to use for OpenNMS authentication
            auth_handler = urllib2.HTTPBasicAuthHandler()
            auth_handler.add_password(realm=self.REALM,
                                      uri=self.url,
                                      user=user,
                                      passwd=password)
            self.opener = urllib2.build_opener(auth_handler)

        self.opener.addheaders = self.DEFAULT_HEADERS.items()

    def path_join(self, *paths):
        "Join some paths together"
        return re.sub(r"\/{2,}", "/", "/".join(paths))

    def get(self, path, params={}):
        """
        Make a generic OpenNMS ReST GET request, and return JSON data

        Example:
        o = OpenNMS()
        nodes = o.get("nodes")
        """

        default_req_params = {
            "limit": 0, # Don't paginate results by default
        }

        # Merge default and specified parameters, and transform non-string param values into strings
        merged_params = dict([(k, str(v)) for k, v in default_req_params.items()] +
                             [(k, str(v)) for k, v in params.items()])

        rest_path = self.path_join(self.api_prefix, path)
        url = "{url}/{path}?{params}".format(url=self.url, path=rest_path, params=urllib.urlencode(merged_params))

        req = urllib2.Request(url)
        response = self.opener.open(req)
        return json.load(response)

    def _method(self, path, method, params={}):
        """
        Make a generic OpenNMS ReST <METHOD> request, and return a file-handle (may or may not have data in it)
        """

        url = "{url}/{path}?{params}".format(url=self.url, path=path, params=urllib.urlencode(params))

        method_header = {"Accept": "application/json, text/plain, */*"}
        req = urllib2.Request(url, headers=method_header)
        req.get_method = lambda: method
        response = self.opener.open(req)
        return response

    def put(self, path, params={}):
        "Make a generic OpenNMS ReST PUT request, and return a file-handle (may or may not have data in it)"
        rest_path = self.path_join(self.api_prefix, path)
        return self._method(rest_path, method="PUT", params=params)

    def post(self, path, payload, headers={}):
        "Make a generic OpenNMS ReST POST request, and return a file-handle (may or may not have data in it)"

        rest_path = self.path_join(self.api_prefix, path)
        url = "{url}/{path}".format(url=self.url, path=rest_path)
        default_post_headers = {"Content-Type": "application/json;charset=UTF-8"}
        merged_headers = dict(default_post_headers.items() + headers.items())

        data = json.dumps(payload, encoding="utf-8")
        req = urllib2.Request(url, data=data, headers=merged_headers)
        response = self.opener.open(req)
        return response

    def delete(self, path, params={}):
        "Make a generic OpenNMS ReST DELETE request, and return a file-handle (may or may not have data in it)"
        rest_path = self.path_join(self.api_prefix, path)
        return self._method(rest_path, method="DELETE", params=params)

    def sync_group(self, name, rescan=False):
        "Load the given group's config"
        group_path = self.path_join("requisitions", name, "import")
        return self.put(path=group_path, params={"rescanExisting": str(rescan).lower()})

    def sync_all(self, rescan=False):
        "Load all group's config"
        requisitions = self.get(path="requisitions")
        for group in requisitions["model-import"]:
            self.sync_group(group["foreign-source"], rescan=rescan)

    def group_exists(self, name):
        "Return True if the given group exists, and False if not"

        try:
            result = self.get(path=self.path_join("requisitions", name))
        except urllib2.HTTPError as err:
            if err.code == 404:
                return False
            raise
        else:
            return True

    def add_group(self, name):
        "Add a requisition group to OpenNMS"

        payload = {"foreign-source": name, "node": []}

        return self.post(path=self.path_join("requisitions", name), payload=payload)

    def add_node(self, name, ip, group="unknown", city="", building="", description="", poll=True):
        """
        Add a node to OpenNMS

        Example:
        o = OpenNMS()
        o.add_node("dev-server-peaches", "10.1.2.4", group="dev-servers", city="AWS", building="us-west-1")
        """

        if not self.group_exists(group):
            print("Creating group {0}".format(group))
            self.add_group(group)

        add_path = self.path_join("requisitions", group, "nodes")

        poll_map = {True: "P", False: "N"}
        foreign_id = "-".join([name, str(int(time.time()))])

        payload = {
            "foreign-id":               foreign_id,
            "node-label":               name,
            "city":                     city,
            "building":                 building,
            "parent-foreign-source":    "",
            "parent-foreign-id":        "",
            "parent-node-label":        None,
            "asset": [{"name": "displayCategory", "value": group},],
            "category": [{"name": group},],
            "interface": [
                {
                    "ip-addr": ip,
                    "snmp-primary": poll_map[poll],
                    "status": "1",
                    "monitored-service": [
                        {"service-name": "ICMP"},
                        {"service-name": "SSH"},
                    ],
                },
            ],
        }

        return self.post(path=add_path, payload=payload)

    def del_node(self, name):
        """
        Delete a node from OpenNMS

        Example:
        o = OpenNMS()
        o.del_node("dev-server-peaches")
        """

        # Scan DB first
        nodes = self.get(path="nodes")
        for node in nodes["node"]:
            if node["label"] == name:
                print("Deleting {0} id {1} from {2}".format(name, node["id"], node["foreignSource"]))
                # Delete from DB
                self.delete(path=self.path_join("nodes", node["id"]))
                # Delete from requisition group
                self.delete(path=self.path_join("requisitions", node["foreignSource"], "nodes", node["foreignId"]))

        # Scan requisitions next (node may not have been in DB)
        requisitions = self.get(path="requisitions")
        for group in requisitions["model-import"]:
            for matching_node in [x for x in group["node"] if x["node-label"] == name]:
                print("Deleting {0} foreign-id {1} from {2}".format(name, matching_node["foreign-id"], group["foreign-source"]))
                self.delete(path=self.path_join("requisitions", group["foreign-source"], "nodes", matching_node["foreign-id"]))

    def del_group(self, name):
        "Delete a requisition group from OpenNMS"

        print("Deleting {0}".format(name))
        self.delete(path=self.path_join("requisitions", name))
        self.delete(path=self.path_join("requisitions", "deployed", name))


if __name__ == "__main__":
    user="admin"
    password="pleasechangeme"
    o = OpenNMS(host="10.1.2.3", user=user, password=pw)

    # Get node info
    #nodes = o.get("nodes")

    # Get requisition info
    #requisitions = o.get("requisitions")

    # Get a bool of if a group exists
    #does_exist = o.group_exists("banana")

    # Add a node to requisition
    #o.add_node("dev-server-peaches", "10.1.2.4", group="dev-servers", city="AWS", building="us-west-1")

    # Apply pending requisition changes (additions)
    #o.sync_group("dev-servers")

    # Delete a node
    #o.del_node("dev-server-peaches")

    # Delete a group
    #o.del_group("dev-servers")
